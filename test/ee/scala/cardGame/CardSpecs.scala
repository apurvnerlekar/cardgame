package ee.scala.cardGame

import org.junit.Test
import org.junit.Assert
import org.mockito.Mockito._
import collection.mutable.ListBuffer

class CardSpecs {

  @Test
  def itComparesTwoCards() {

    //given
    val mockDeckBuilder = mock(classOf[DeckBuilder])
    val mockDeck = mock(classOf[Deck])

    when(mockDeckBuilder buildDeck) thenReturn mockDeck
    when(mockDeck shuffle) thenReturn ListBuffer(new Card(CardSuite Spade, CardFaceValue Eight), new Card(CardSuite Club, CardFaceValue Ace))

    val deck = mockDeckBuilder buildDeck
    val shuffledDeck = deck shuffle

    val card1 = shuffledDeck(0)
    val card2 = shuffledDeck(1)
    //when
    val actual1 = card1.compare(card2)
    val actual2 = card2.compare(card2)
    //then
    Assert assertEquals(actual1, -1)
    Assert assertEquals(actual2, 0)
  }

}
