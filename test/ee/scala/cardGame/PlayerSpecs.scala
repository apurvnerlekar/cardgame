package ee.scala.cardGame

import org.junit.{Assert, Test}

class PlayerSpecs {

  @Test
  def itAcceptsCard() {

    //given
    val player = new Player
    val card = new Card(CardSuite Diamond, CardFaceValue Ace)

    //when
    player accept card

    //then
    Assert assertEquals(card, player cards 0)
  }

  @Test
  def itDrawsCard() {

    //given
    val player = new Player
    val card = new Card(CardSuite Diamond, CardFaceValue Ace)
    player accept card

    //when
    val drawnCard = player drawCard

    //then
    Assert assertEquals(card, drawnCard)
  }

}
