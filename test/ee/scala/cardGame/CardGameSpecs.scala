package ee.scala.cardGame

import org.junit.{Before, Assert, Test}
import org.mockito.Mockito._
import collection.mutable.ListBuffer

class CardGameSpecs {
  var mockDeck: Deck = mock(classOf[Deck])
  var mockDeckBuilder: DeckBuilder = mock(classOf[DeckBuilder])
  var deck: Deck = new Deck

  @Before
  def initialize() {

    when(mockDeckBuilder buildDeck) thenReturn mockDeck
    when(mockDeck shuffle) thenReturn ListBuffer(new Card(CardSuite Spade, CardFaceValue Queen), new Card(CardSuite Spade, CardFaceValue King), new Card(CardSuite Club, CardFaceValue Four), new Card(CardSuite Diamond, CardFaceValue Seven), new Card(CardSuite Spade, CardFaceValue Six), new Card(CardSuite Spade, CardFaceValue Two), new Card(CardSuite Club, CardFaceValue Nine), new Card(CardSuite Diamond, CardFaceValue Ten), new Card(CardSuite Spade, CardFaceValue Three), new Card(CardSuite Spade, CardFaceValue Ace), new Card(CardSuite Club, CardFaceValue Ace), new Card(CardSuite Diamond, CardFaceValue Jack), new Card(CardSuite Spade, CardFaceValue King), new Card(CardSuite Spade, CardFaceValue Queen), new Card(CardSuite Club, CardFaceValue Jack), new Card(CardSuite Diamond, CardFaceValue Two), new Card(CardSuite Spade, CardFaceValue Three), new Card(CardSuite Spade, CardFaceValue Four), new Card(CardSuite Club, CardFaceValue Five), new Card(CardSuite Diamond, CardFaceValue Six), new Card(CardSuite Spade, CardFaceValue Seven), new Card(CardSuite Spade, CardFaceValue Eight), new Card(CardSuite Club, CardFaceValue Ten), new Card(CardSuite Diamond, CardFaceValue Ace))
    deck = mockDeckBuilder buildDeck
  }

  @Test
  def itDistributesTheCardToSinglePlayer() {

    //given
    val shuffledDeck = deck shuffle
    val expected = shuffledDeck(0)
    val players = List(new Player)
    val cardGame = new CardGame(players)

    //when
    cardGame distributeCardTo(players, shuffledDeck)
    //then
    Assert assertEquals(expected, players(0) cards 0)
  }

  @Test
  def itDistributesTheCardsToMultiplePlayers() {

    //given
    val shuffledDeck = deck shuffle
    val expected0 = shuffledDeck(0)
    val expected1 = shuffledDeck(1)
    val expected2 = shuffledDeck(2)
    val expected3 = shuffledDeck(3)

    val players = List(new Player, new Player, new Player, new Player)
    val cardGame = new CardGame(players)

    //when
    cardGame distributeCardTo(players, shuffledDeck)

    //then
    Assert assertEquals(expected0, players(0) cards 0)
    Assert assertEquals(expected1, players(1) cards 0)
    Assert assertEquals(expected2, players(2) cards 0)
    Assert assertEquals(expected3, players(3) cards 0)
  }

  @Test
  def itPlaysGameForSingleRoundWithSingleCard() {

    //given
    val shuffledDeck = deck shuffle
    val allPlayers = List(new Player, new Player, new Player, new Player)
    val cardGame = new CardGame(allPlayers)
    cardGame distributeCardTo(allPlayers, shuffledDeck)

    //when
    val winningPlayer = cardGame play 1

    //then
    Assert assertEquals(allPlayers(3), winningPlayer)
  }

  @Test
  def itPlaysGameForFiveRoundsWithFiveCards() {

    //given
    val shuffledDeck = deck shuffle
    val allPlayers = List(new Player, new Player, new Player, new Player)
    val cardGame = new CardGame(allPlayers)

    (0 to 4) foreach {
      time =>
        cardGame distributeCardTo(allPlayers, shuffledDeck)
    }

    //when
    val winningPlayer = cardGame play 5

    //then
    Assert assertEquals(allPlayers(3), winningPlayer)
  }
}
