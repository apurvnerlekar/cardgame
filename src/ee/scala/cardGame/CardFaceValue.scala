package ee.scala.cardGame

object CardFaceValue extends Enumeration {
  type CardFaceValue = Value

  val Ace, Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King = Value
}
