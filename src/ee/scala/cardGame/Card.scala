package ee.scala.cardGame

class Card(suite1: CardSuite.Value, value1: CardFaceValue.Value) extends Ordered[Card] {
  val suite = suite1
  val value = value1

  def compare(that: Card): Int = {
    if ((this.suite compareTo (that.suite)) == 0) {
      return this.value compareTo (that.value)
    }
    -1
  }
}
