package ee.scala.cardGame

object CardSuite extends Enumeration {
  type CardSuite = Value

  val Diamond, Heart, Club, Spade = Value
}

